import { config } from './costants.widget.js';
import { env } from './env.js';

import { playToNav, timeControl, ShowIp, intervalTimeControl, validateName, validateDoc,
    validatePhone, validateEmail, validatePolicies, openLime, MessageSystem, decrypt
} from './function.widget.js';

config.authLogin(decrypt(env.USER_GATEWAY), decrypt(env.PASSWOR_GATEWAY));
$(function(){
    if (!window._genesys) window._genesys = {};
    if (!window._gt) window._gt = [];

    /**
    * Arreglo para construccion del widget y las funcionalidades del chat

    * @date 06/12/2021
    * @author Maycol David Sánchez Mora
    */
    window._genesys.widgets =
    {
        main: {
            debug: false,
            theme: "light",
            lang: "es",
            emojis: true,
            actionsMenu: false,
            mobileMode: 'auto',
            mobileModeBreakpoint: 1000,
            i18n: config.urlDomain + 'js/lang.json',
            header: {
                'Authorization': config.token
            },
            cookieOptions: {
                secure: true,
                sameSite: 'Strict'
            }
        },
        webchat: {
            apikey: "",
            dataURL:  config.urlSkill,
            emojis: true,
            actionsMenu: true,
            uploadsEnabled: true,
            enableCustomHeader: true,
            chatButton: {
                enabled: true,
                template: false,
                openDelay: 1000,
                effectDuration: 300,
                hideDuringInvite: true
            },
            form: {
                wrapper: "<div></div>",
                inputs: [
                    {
                        id: "cx_webchat_form_lastname",
                        name: "lastname",
                        maxlength: "100",
                        // placeholder: "lastname",
                        type: "hidden",
                        value: '-'
                    },
                    {
                        id: "cx_webchat_form_firstname",
                        name: "firstname",
                        maxlength: "30",
                        placeholder: "Nombre completo",
                        span: "cx-error",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: function (event) {
                            if (event != 0) {
                                return validateName();
                            }
                        }

                    },
                    {
                        id: "cx_webchat_form_doc",
                        name: "Identify",
                        maxlength: "16",
                        placeholder: "Documento",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: function (event) {
                            if (event != 0) {
                                return validateDoc();
                            }
                        }
                    },
                    {
                        id: "cx_webchat_form_phone",
                        name: "PhoneNumber",
                        maxlength: "10",
                        placeholder: "Celular",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='row'><div class='col col-12'>{input}</div></div>",
                        validate: function (event) {
                            if (event != 0) {
                                return validatePhone();
                            }
                        }
                    },
                    {
                        id: "cx_webchat_form_email",
                        name: "email",
                        maxlength: "60",
                        placeholder: "Correo Electronico",
                        type: "Email",
                        span: "cx-error",
                        validateWhileTyping: false,
                        autocomplete: "off",
                        wrapper: "<div class='row mb-20'><div class='col col-12'>{input}</div></div>",
                        validate: function (event) {
                            if (event != 0) {
                                return validateEmail();
                            }
                        }
                    },
                    {
                        id: "cx_webchat_form_iporigen",
                        name: "IpOrigen",
                        maxlength: "100",
                        type: "hidden",
                        value: ""
                    },
                    {
                        id: "cx_webchat_form_subject",
                        name: "subject",
                        maxlength: "100",
                        placeholder: "subject",
                        type: "hidden",
                        value: config.stringTypeChat
                    },
                    {
                        id: "cx_webchat_form_transcripcion",
                        name: "FiltroCorreo",
                        maxlength: "100",
                        placeholder: "transcripcion",
                        type: "hidden",
                        value: "true"
                    },
                    {
                        id: "cx_webchat_form_otro",
                        name: "Otro",
                        maxlength: "100",
                        type: "hidden",
                        value: "Otro campo"
                    },
                    {
                        id: "cx_webchat_form_politicas",
                        name: "sitio",
                        type: "checkbox",
                        span: "cx-error",
                        value: "true",
                        label: "Aceptar <a href='https://fondokonecta.com.co/pdfs/POLITICA-DE-DATOS-PERSONALES.pdf' title='Términos y Condciones' target='_blank'>Términos y Condiciones</a>",
                        wrapper: "<div class='row mb-20'><div class='col col-12'>{input} {label}</div></div>",
                        validate: function (event, form, input) {
                            if (event != 0) {
                                return validatePolicies(event, input);
                            }
                        }
                    }
                ]
            },
            markdown: false
        }
    };

    if(!window._genesys.widgets.extensions){
        window._genesys.widgets.extensions = {};
    }

    /**
    * Funcion para la creacion de una extencion del widget para poder ejecutar comandos y suscribirse a los eventos
    *
    * @date 06/12/2021
    * @author Maycol David Sánchez Mora
    */
    window._genesys.widgets.extensions["MyPlugin"] = function($){

        var oMyPlugin = window._genesys.widgets.bus.registerPlugin('MyPlugin');

        /**
        * Suscripcion al evento que abre el chat, se valida el control de horario, y se cuarga la ip del cliente
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChat.opened', function(){
            timeControl().then(scheduleValid => {
                if(!scheduleValid.booleanValid){
                    $('.cx-webchat .cx-body').html(config.htmlTimeControl);
                    $('.cx-webchat').addClass('controlHorario');
                } else{
                    $('#cx_webchat_form_firstname').focus();
                    ShowIp();
                    intervalTimeControl(scheduleValid.intMiliseg);
                    getSessionData();
                }
            });
        });

        /**
        * Suscripcion al evento de los mensajes recibidos, se valida el control de horario, y se cuarga la ip del cliente
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.messageReceived', function(e){
            e.data.messages.forEach(element => {
                custom_menssage(element);
            });
        });

        /**
        * Suscripcion al evento de chat completado, este evento se ejecuto cuando el chat finaliza y se hubo interacion con el agentes
        * si el chat presenta encuesta en esta opcion se realizaria el llamado de esta
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChat.completed', function(){
            if(!config.userData) {
                $.ajax({
                    type: 'POST',
                    async: true,
                    url: config.urlSkill + '/getData',
                    headers: {
                        "Authorization": config.token
                      },
                    data:
                    {
                        "SesionID": config.SessionID
                    },
                    dataType: "json",
                    success: function(resp){
                        config.userData = resp.data.user_data;
                        openLime(config.userData);
                    }
                });
            } else {
                openLime(config.userData);
            }
            setInterval(()=>{
                oMyPlugin.command('WebChat.close');
            },30000);
        });

        /**
        * Suscripcion al evento de chat finalizado, este evento se ejecuto cuando el chat finaliza independientemente de si hubo o no conexion con el agente
        * si el chat presenta encuesta en esta opcion se realizaria el llamado de esta
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.ended', function(){
            if(config.intAgentConected  != 1){
                oMyPlugin.command('WebChat.close');
            }
            config.intAgentConected  = 0;
        });

        /**
        * Comando de chat pre registro de informacion, en este comando se realiza la validacion que el campo de mensajes del chat no envie direcciones web
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.command('WebChatService.registerPreProcessor', { preprocessor: function(message){
            const texto = ""+message.text;
            const url = texto.match(config.regexUrl);
            const email = texto.match(config.regexEmail);

            if (url !== null && email == null) {
                const text_replace = texto.replace(url[0], '').replace('https://', '').replace('http://', '');
                message.text = text_replace;
                $("#cx_input").val(text_replace);
                window._genesys.widgets.common.showAlert(
                    $('.cx-widget.cx-webchat'),
                    {
                        text: 'No se permite enviar direciones url al chat', buttonText: 'Ok'
                    }
                );
            } else {
                const text_replace = texto.replace('https://', '').replace('http://', '');
                message.text = text_replace;
            }
            return message;
        }});

        /**
        * Suscripcion al evento de chat cuando un cliente se conecta, en este evento se agregan los mensajes de espera mientras un agente logra conectarse
        *
        * @date 07/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.clientConnected', function(){
            config.inter3 = setInterval(()=>{
                if(!config.booleanWait){
                    clearInterval(config.inter3);
                    MessageSystem("En un momento un asesor te atenderá.");
                }
            },20000);

            config.inter2 = setInterval(()=>{
                if(!config.booleanWait && !config.booleanMsgValid){
                    config.booleanMsgValid = true;
                    MessageSystem("Por favor continua en linea. En un momento un asesor te atenderá.");
                }
            },120000);

            config.inter2 = setInterval(()=>{
                if(!config.booleanWait){
                    MessageSystem("Estamos buscando un asesor para atender tu solicitud por favor permanece en línea.");
                }
            },150000);
        });

        /**
        * Suscripcion al evento de chat cuando un agente se conecta, en este evento se agregan los mensajes de espera mientras un agente logra conectarse
        *
        * @date 07/12/2021
        * @author Maycol David Sánchez Mora
        */
        oMyPlugin.subscribe('WebChatService.agentConnected', function(){
            config.intAgentConected  = 1;
            playToNav();
            clearInterval(config.inter2);
            config.booleanWait= true;
        });

        oMyPlugin.subscribe('WebChat.started', function(e){
            config.userData = e.data.form;
            $.ajax({
                type: 'POST',
                async: true,
                url: config.urlSkill + '/saveUser',
                headers: {
                    "Authorization": config.token
                  },
                data:
                {
                    "SesionID": config.SessionID,
                    "UserData": config.userData
                },
                dataType: "json",
                success: function(){
                    //Something
                }
            });
        });

        oMyPlugin.subscribe('WebChatService.started', function(e){
            config.SessionID = e.data.sessionID;
        });

        /**
        * Comando para obtener los datos de session del chat
        *
        * @date 06/12/2021
        * @author Maycol David Sánchez Mora
        */
        function getSessionData() {
            /**
            * Comando para obtener los datos de session del chat
            *
            * @date 06/12/2021
            * @author Maycol David Sánchez Mora
            */
            oMyPlugin.command('WebChatService.getSessionData').done(ex => {
                if(ex.sessionID != "") {
                    oMyPlugin.command('WebChatService.getAgents').done(function(ega) {
                        $.each(ega.agents, function(value) {
                            if (value.connected) {
                                config.intAgentConected  = 1;
                            }
                        });
                    });
                }
            });
        }


        oMyPlugin.command('WebChatService.restore').done(function(e){
                config.SessionID =  e.chatId;
            }).fail(function(){
                // WebChatService failed to restore
            });

    };

    /**
    * Ajax setup para agregar los encabezados a todas las peticiones
    *
    * @date 07/12/2021
    * @author Maycol David Sánchez Mora
    */
    $.ajaxSetup({
        beforeSend: function (xhr){
           xhr.setRequestHeader("Cache-Control","must-revalidate, no-cache, no-store, private");
           xhr.setRequestHeader("Pragma","no-cache");
        }
    });

    /**
    * Eventos keyup jquery para validar los tipos de datos (Si es numerico solo permitira ingrsar numeros)
    *
    * @date 07/12/2021
    * @author Maycol David Sánchez Mora
    */
    $("body").on('keyup', '#cx_webchat_form_phone', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    }).on('keyup', '#cx_webchat_form_doc', function(){
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });

    /**
    * Funcion para la creacion dinamica de los script y css necesarios para el chat
    *
    * @date 07/12/2021
    * @author Maycol David Sánchez Mora
    */
    (function (o) {
        var f = function () {
            var d = o.location;
            o.aTags = o.aTags || [];
            o.aTags.forEach(oTag => {
                var fs = d.getElementsByTagName(oTag.type)[0], e;
                if (!d.getElementById(oTag.id)) {
                    e = d.createElement(oTag.type);
                    e.id = oTag.id;
                }
                if (oTag.type == "script") {
                    e.src = oTag.path;
                }
                else {
                    e.type = 'text/css'; e.rel = 'stylesheet'; e.href = oTag.path;
                }
                if(oTag.integrity){
                    e.integrity = oTag.integrity;
                    e.setAttribute('crossorigin', 'anonymous');
                }
                if (fs) {
                    fs.parentNode.insertBefore(e, fs);
                } else {
                    d.head.appendChild(e);
                }
            });
        }, ol = window.onload;
        if (o.onload) {
            typeof window.onload != "function" ? window.onload = f : window.onload = function () { ol(); f(); };
        }
        else f();
    })
    ({
        location: document,
        onload: false,
        aTags:
            [
                {
                    type: "script",
                    id: "genesys-cx-widget-script",
                    path: config.urlDomain + "js/widgets.min.js",
                    integrity: false
                },
                {
                    type: "link",
                    id: "genesys-cx-widget-styles",
                    path: config.urlDomain + "css/widgets.min.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-chats-styles",
                    path: config.urlDomain + "css/webchat.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-menu-styles",
                    path: config.urlDomain + "css/widgets.menu.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-icons-styles",
                    path: config.urlDomain + "css/icons.css",
                    integrity: false
                },
                {
                    type: "link",
                    id: "widget-rows-styles",
                    path: config.urlDomain + "css/rows.min.css",
                    integrity: false
                }
            ]
    });
});

/**
* funcion para los mensajes personalidos de espera y la finalizacion del chat por inactividad
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param object element
* @return NULL
*/
function custom_menssage(element) {
    if(element.type=="Message" && element.from.type=="Agent" ){
        playToNav();
        config.booleanWrite = false;
        clearInterval(config.inter1);
        config.inter1 = setInterval(() => {
            if (!config.booleanWrite) {
                config.intCounter++;
                var strMsg = '';
                if (config.intCounter == 18 && config.intAgentConected  == 1 && !config.booleanChatEnd) {
                    strMsg = `¿Sigues en línea con nosotros? 
                        No hemos identificado actividad en el chat, si no tenemos algún mensaje de tu parte la conversación finalizará automáticamente`;
                    MessageSystem(strMsg);
                }
                if (config.intCounter == 30 && config.intAgentConected  == 1 && !config.booleanChatEnd) {
                    clearInterval(config.inter1);
                    config.booleanWrite = true;
                    MessageSystem();
                    strMsg  = `No hemos identificado actividad durante los últimos 5 minutos, a continuación la conversación finalizará. 
                        Gracias por haberte comunicado con nosotros, si tienes alguna duda adicional, te invitamos a que nos contáctes de nuevo a través de este canal.`;
                    MessageSystem(strMsg);
                    setTimeout(() => {
                        config.intCounter = 0;
                        /**
                         * IMPORTATE!
                         * INFORMACION A SEGURIDAD:
                         * Esta variable es una variable propia de la librería de genesys y es creada en el archivo widgets.min.js (Librería de genesys)
                         */
                        window._genesys.widgets.bus.command('WebChatService.endChat');
                    }, 5000);
                }
            }
        }, 1000);
    } else if(element.from.type=="Client" && element.type=="Message"){
        config.intCounter = 0;
        clearInterval(config.inter1);
        config.booleanWrite = true;
    } else {
        //Somethig
    }
}
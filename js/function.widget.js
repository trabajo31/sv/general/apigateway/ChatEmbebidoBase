import { config } from './costants.widget.js';
/** Variables control de horario */
var clickSound = createSoundBite("sound/beep.mp3", "sound/beep.wav");

export function encrypt(text) {
    var salt = CryptoJS.lib.WordArray.random(256);
    var iv = CryptoJS.lib.WordArray.random(16);

    var key = CryptoJS.PBKDF2('dfeccaqsdia83BjxGXo75eGG3Iw3hraL', salt, { hasher: CryptoJS.algo.SHA512, keySize: 64/8, iterations: 999 });

    var encrypted = CryptoJS.AES.encrypt(text, key, {iv: iv});

    var data = {
        ciphertext : CryptoJS.enc.Base64.stringify(encrypted.ciphertext),
        salt : CryptoJS.enc.Hex.stringify(salt),
        iv : CryptoJS.enc.Hex.stringify(iv)    
    };

    return window.btoa(JSON.stringify(data));
}

export function decrypt(jsonstring) {
    try {
      var obj_json = JSON.parse(window.atob(jsonstring));

      var encrypted = obj_json.ciphertext;
      var salt = CryptoJS.enc.Hex.parse(obj_json.salt);
      var iv = CryptoJS.enc.Hex.parse(obj_json.iv);   
  
      var key = CryptoJS.PBKDF2('dfeccaqsdia83BjxGXo75eGG3Iw3hraL', salt, { hasher: CryptoJS.algo.SHA512, keySize: 64/8, iterations: 999});
      var decrypted = CryptoJS.AES.decrypt(encrypted, key, { iv: iv});
  
      return decrypted.toString(CryptoJS.enc.Utf8);
    } catch (error) {  
      return '';
    }
   
  }

/**
* Funcion para la creacion de mensajes de sistemas
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param string stringMsg
*/
export function MessageSystem(stringMsg){
    window._genesys.widgets.bus.command('WebChat.injectMessage', {
        type: 'text',
        text: stringMsg,
        custom: false
    });
}

//Funcion para ontener fecha server
/**
* Funcion para obtener fecha desde la api horario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return date dateTime
*/
function timeServer(){
    let dateTime;
    // $.ajax({
    //     type: 'POST',
    //     async: false,
    //     url: config.urlTimeServer,
    //     data: null,
    //     dataType: "json",
    //     success: function(data){
    //         dateTime = data;
    //     }
    // });
    // return dateTime;
    return {
        "year": 2022,
        "month": 4,
        "day": 25,
        "hour": 12,
        "minutes": 35,
        "seconds": 20
    };
}

/**
* Funcion que define el intervalo el control de horario llamado a la funcion intervalFuntionTimeControl que ejecuta la consulta del control de horario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function intervalTimeControl(intMilseg){
    config.intervalTimeC = setInterval(() => {
        intervalFuntionTimeControl();
    }, intMilseg);
}

/**
* Funcion que ejecuta el control de horario llamado desde la funcion de intervalo intervalTimeControl
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return promise
*/
function intervalFuntionTimeControl(){
    return new Promise(resolve => {
        timeControl().then(arraytimeControl => {
            clearInterval(config.intervalTimeC);
            if(!arraytimeControl.booleanValid){
                window._genesys.widgets.bus.command('WebChatService.getSessionData').done(ex => {
                    if(ex.sessionID == "") {
                        $('.cx-webchat .cx-body').html(config.htmlTimeControl);
                        $('.cx-webchat').addClass('timeControl');
                    } else {
                        MessageSystem("no se pudo asignar un asesor, se finalizara la interacion debido a que estamos fuera de horario");
                        setTimeout(() => {
                            window._genesys.widgets.bus.command('WebChatService.endChat');
                        }, 8000);
                    }
                });
            } else {
                intervalTimeControl(arraytimeControl.intMiliseg);
            }
            resolve();
        });
    });
}

/**
* Funcion para Validar el Control de Horario.
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return promise arraytimeControl
*/
export function timeControl() {
    return new Promise(resolve => {
        var arraytimeControl = {};
        var dateTimeServer = timeServer();
        var dateNow = new Date();
        var dateWeek = parseInt(dateNow.getUTCDay()) + 1;
        var dateHour = parseInt(dateTimeServer.hour);
        var dateMinutes = parseInt(dateTimeServer.minutes);
        var dateSeconds = parseInt(dateTimeServer.seconds);
        var dateHourActualy = prefix(dateHour)+":"+prefix(dateMinutes)+":"+prefix(dateSeconds);

        if (dateWeek == 1 ) {
            arraytimeControl =  {
                booleanValid: false
            };
        } else if (dateWeek == 7) {
            if (dateHourActualy < config.timeControlWeekend[0] || dateHourActualy >= config.timeControlWeekend[1]) {
                arraytimeControl = {
                    booleanValid: false
                };
            }else{
                arraytimeControl = {
                    booleanValid: true,
                    intMiliseg: calculateDifference(dateHourActualy, config.timeControlWeekend[2])
                };
            }
        } else {
            if (dateHourActualy < config.timeControlWeek[0] || dateHourActualy >= config.timeControlWeek[1]) {
                arraytimeControl = {
                    booleanValid: false
                };
            } else {
                arraytimeControl = {
                    booleanValid: true,
                    intMiliseg: calculateDifference(dateHourActualy, config.timeControlWeek[2])
                };
            }
        }
        resolve(arraytimeControl);
    });
}

/**
* Funcion para obtener la ip del usuario
*
* @date 07/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function ShowIp() {
    setTimeout(() => {
        $(function getip() {
            $.getJSON("https://api.ipify.org?format=jsonp&callback=?",
                function(json) {
                    $("#cx_webchat_form_iporigen").val(json.ip);
                }
            );
        });
    }, 500);
}

/**
* Funcion para validar el nombre del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidName
*/
export function validateName() {
    let booleanValidName = false;
    var x = $("#cx_webchat_form_firstname").val();
    if (x == "") {
        alertName(booleanValidName);
    } else {
        if (validateTypeText(x)) {
            if(validateLenght("cx_webchat_form_firstname", "alertnamedv", 6, 30)){
                if($('#alertnamedv').length){
                    $('#alertnamedv').remove();
                }
                booleanValidName = true;
            }
        } else {
            alertName(booleanValidName);
        }
    }
    return booleanValidName;
}

/**
* Funcion para validar el documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidIdentify
*/
export function validateDoc() {
    let booleanValidIdentify = false;
    var x = $("#cx_webchat_form_doc").val();
    if (x == "") {
        alertIndentify(booleanValidIdentify);
    } else {
        if (validateTypeNumber(x)) {
            if(validateLenght("cx_webchat_form_doc", "alertidentifydv", 7, 16)){
                if($('#alertidentifydv').length){
                    $('#alertidentifydv').remove();
                }
                booleanValidIdentify = true;
            }
        } else {
            alertIndentify(booleanValidIdentify);
        }
    }
    return booleanValidIdentify;
}

/**
* Funcion para validar el numero de telefono del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidPhone
*/
export function validatePhone() {
    let booleanValidPhone = false;
    var x = $("#cx_webchat_form_phone").val();
    if (x == "") {
        alertPhone(booleanValidPhone);
    } else {
        if (validateTypeNumber(x)) {
            if(validateLenght("cx_webchat_form_phone", "alertphonedv", 3, 10)){
                if($('#alertphonedv').length){
                    $('#alertphonedv').remove();
                }
                booleanValidPhone = true;
            }
        } else {
            alertPhone(booleanValidPhone);
        }
    }
    return booleanValidPhone;
}

/**
* Funcion para validar el email del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidEmail
*/
export function validateEmail() {
    let booleanValidEmail = false;
    var x = $("#cx_webchat_form_email").val();
    if (x === "") {
        alertEmail(booleanValidEmail);
    } else {
        if(!validateTypeEmail(x)){
            alertEmail(booleanValidEmail);
        }else{
            if(validateLenght("cx_webchat_form_email", "alertemaildv", 7, 60)){
                if($('#alertemaildv').length){
                    $('#alertemaildv').remove();
                }
                booleanValidEmail = true;
            }
        }
    }
    return booleanValidEmail;
}

/**
* Funcion para validar el numero el aceptamiento de politicas del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return boolean: booleanValidEmail
*/
export function validatePolicies(event, input) {
    let booleanValidPoliticas = true;
    if (event.type != 'blur' || $.isEmptyObject(event)) {
        const checkedPolicies = $(input).is(':checked');
        if(!checkedPolicies){
            window._genesys.widgets.common.showAlert(
                $('.cx-widget.cx-webchat'),
                {
                    text: 'Para continuar es necesario leer, entender ' +
                    'y autorizar la Política de Tratamiento de Datos',
                    buttonText: 'Ok'
                }
            );
            booleanValidPoliticas = false;
        }
        return booleanValidPoliticas;
    }
}

/**
* Funcion para validar el tamaño de los campos
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: input
* @param string: inputAlert
* @param int: intMin
* @param int: intMax
* @return boolean: booleanValid
*/
function validateLenght(input, inputAlert, intMin, intMax) {
    var x = $("#"+input).val();
    var booleanValid = true;
    if(x.length < intMin || x.length > intMax){
        alertLenght(input, inputAlert, intMin, intMax);
        booleanValid = false;
    }
    return booleanValid;
}

/**
* Funcion para mostrar el mensaje de validacion del campo nombre del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidName
* @return NULL
*/
function alertName(booleanValidName){
    if(!booleanValidName){
        if($('#alertnamedv').length){
            $('#alertnamedv').remove();
        }
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Introduce tu nombre, recuerda que este campo debe contener solo texto.");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertnamedv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_firstname').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo documento del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidIdentify
* @return NULL
*/
function alertIndentify(booleanValidIdentify) {
    if (!booleanValidIdentify) {
        if($('#alertidentifydv').length){
            $('#alertidentifydv').remove();
        }
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Por favor ingresa tu identificación");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertidentifydv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_doc').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo telefono del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidPhone
* @return NULL
*/
function alertPhone(booleanValidPhone){
    if(!booleanValidPhone){
        if($('#alertphonedv').length){
            $('#alertphonedv').remove();
        }
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Parece que has introducido un número invalido, recuerda que este campo debe ser numérico");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertphonedv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_phone').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion del campo email del usuario
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param boolean: booleanValidEmail
* @return NULL
*/
function alertEmail(booleanValidEmail){
    if(!booleanValidEmail){
        if($('#alertemaildv').length){
            $('#alertemaildv').remove();
        }
        var spanName = document.createElement("div");
        var contentSpan = document.createTextNode("Parece que has introducido un correo invalido, por favor revisa la información.");
        spanName.style.cssText = 'color:red';
        spanName.id = 'alertemaildv';
        spanName.classList.add("wc-error");
        spanName.appendChild(contentSpan);

        $('#cx_webchat_form_email').parent('.col').append(spanName);
    }
}

/**
* Funcion para mostrar el mensaje de validacion de los tamaños de los campos
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: input
* @param string: inputAlert
* @param int: intMin
* @param int: intMax
* @return NULL
*/
function alertLenght(input, inputAlert, intMin, intMax) {
    if($('#'+inputAlert).length){
        $('#'+inputAlert).remove();
    }
    var spanName = document.createElement("div");
    var contentSpan = document.createTextNode("El campo debe contener entre "+intMin+" y "+intMax+" caracteres");
    spanName.style.cssText = 'color:red';
    spanName.id = inputAlert;
    spanName.classList.add("wc-error");
    spanName.appendChild(contentSpan);

    $('#'+input).parent('.col').append(spanName);
}

/**
* Funcion para validar que un dato sea de tipo numerico
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNumber
* @return boolean: booleanValid
*/
function validateTypeNumber(intNumber) {
    var checkOK = "0123456789" + "0123456789";
    var checkStr = intNumber;
    var booleanValid = true;

    for (let i = 0; i < checkStr.length; i++) {
        const ch = checkStr.charAt(i);
        let j = 0;
        for (j; j < checkOK.length; j++) {
            if (ch == checkOK.charAt(j))
                break;
        }
        if (j == checkOK.length) {
            booleanValid = false;
            break;
        }
    }

    return booleanValid;
}

/**
* Funcion para validar que un dato sea de tipo string
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: stringText
* @return boolean: booleanValid
*/
function validateTypeText(stringText) {
    var filter = /^[A-Za-z áéíóúñüàè\_\-\.\s\xF1\xD1]+$/;
    var booleanValid = false;
    if (filter.test(stringText)) {
        booleanValid = true;
    }

    return booleanValid;
}

/**
* Funcion para validar que un dato sea de tipo string
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param string: email
* @return boolean
*/
function validateTypeEmail(email) {
    const re = config.regexEmailFunction;
    return re.test(email);
}

/**
* Funcion para validar el navegador y ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
export function playToNav() {
    if (navigator.appName == "Microsoft Internet Explorer") {
        playSound();
    }
    else {
        clickSound.playclip();
    }
}

/**
* Funcion para ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
function playSound() {
    var thisSound = document.getElementById("beep");
    thisSound.Play();
}

/**
* Funcion para ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param NULL
* @return NULL
*/
function createSoundBite(sound) {
    var html5audio = document.createElement('audio');
    if (html5audio.canPlayType) { //check support for HTML5 audio
        for (const valArr of arguments) {
            var sourceel = document.createElement('source');
            sourceel.setAttribute('src', valArr);
            if (valArr.match(/\.(\w+)$/i))
                sourceel.setAttribute('type', config.html5_audiotypes[RegExp.$1]);
            html5audio.appendChild(sourceel);
        }
        html5audio.load();
        html5audio.playclip = function () {
            html5audio.pause();
            html5audio.currentTime = 0;
            html5audio.play();
        };
        return html5audio;
    }
    else {
        return {
            playclip: function () {
                throw new Error("Your browser doesn't support HTML5 audio unfortunately");
            }
        };
    }
}

/**
* Funcion para ejecutar el sonido del beep al momento que llega un mensaje
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNum
* @return string: intNum
*/
function prefix(intNum) {
    return intNum < 10 ? ("0" + intNum) : intNum;
}

/**
* Funcion para calcular la dieferencia entre horas
*
* @date 11/12/2021
* @author Maycol David Sánchez Mora
* @param int: intNum
* @return string: intNum
*/
function calculateDifference(dateInitDate, dateEndDate){
    var conversionTable = {
        intSeconds: 1000,
        intMinutes: 60*1000,
        intHours: 60*60*1000
    };

    var convertTime = opts =>
        Object.keys(opts).reduce((end, timeKey) => (
            end + opts[timeKey] * conversionTable[timeKey]
        ), 0);

    var dateInit = new Date("01/01/2000 "+dateInitDate);
    var dateEnd = new Date("01/01/2000 "+dateEndDate);

    var intDiff = dateEnd.getTime() - dateInit.getTime();

    var intMsec = intDiff;
    var intHours = Math.floor(intMsec / 1000 / 60 / 60);
    intMsec -= intHours * 1000 * 60 * 60;
    var intMinutes = Math.floor(intMsec / 1000 / 60);
    intMsec -= intMinutes * 1000 * 60;
    var intSeconds = Math.floor(intMsec / 1000);

    return convertTime({
        intHours: prefix(intHours),
        intMinutes: prefix(intMinutes),
        intSeconds: prefix(intSeconds)
    });

}

/**
* Funcion para abrir la encuesta del chat
*
* @date 11/12/2021
* @author Luis Hernandez Jimenez
* @param int: intNum
* @return string: intNum
*/
export function openLime(userData) {
    const chat_documento = userData.Identify;
    const chat_email = userData.email;
    const chat_nombre = userData.firstname;
    const fecha = new Date();
    const strMessage = "https://limesurvey2.grupokonecta.local/lime_calidad/allusIntegracionChat.php?id_encuesta=37798&id_interaccion=" + fecha + "&identificacion_cliente=" + chat_documento + "&nombre_cliente=" + chat_nombre + "&correo=" + chat_email;
    window.open(strMessage);
}
import { env } from './env.js';
export const config = {
    // urlSkill: 'https://desarrollodbacklog.grupokonecta.local/authsv/api/host',
    // urlLogin: 'https://desarrollodbacklog.grupokonecta.local/authsv/api',
    urlSkill: 'http://127.0.0.1:8000/api/host',
    urlLogin: 'http://127.0.0.1:8000/api',
    urlTimeServer: 'https://asistenciawebv2-dev.grupokonecta.co:5005/api_horario/hora',
    credentials: {
        user: env.USER_GATEWAY,
        password: env.PASSWORD_GATEWAY
    },
    authLogin: function authLogin(user, password) {
        $.ajax({
            type: 'POST',
            async: false,
            url: config.urlLogin + '/auth/loginService',
            data:
            {
                "service_user": user,
                "password": password
            },
            dataType: "json",
            success: function(data){
                config.token = 'Bearer ' + data.data.token;
            }
        });
    },
    /**
     * IMPORTATE!
     * INFORMACION A SEGURIDAD:
     * Esta es una validación de regla de negocios
     */
    regexUrl: /[a-z0-9\-\.]+[^www\.](\.[a-z]{2,4})[a-z0-9\/\-\_\.]*/i,
    /**
     * IMPORTATE!
     * INFORMACION A SEGURIDAD:
     * Esta es una validación de regla de negocios
     */
    regexEmail: /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
    /**
     * IMPORTATE!
     * INFORMACION A SEGURIDAD:
     * Esta es una validación de regla de negocios
     */
    regexEmailFunction: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    urlDomain: '',
    html5_audiotypes: {
        "mp3": "audio/mpeg",
        "mp4": "audio/mp4",
        "ogg": "audio/ogg",
        "wav": "audio/wav"
    },
    intervalTimeC: null,
    timeControlWeek: ["07:00", "18:00", "18:00:00"],
    timeControlWeekend: ["07:00", "12:00", "12:00:00"],
    inter1: null,
    inter2: null,
    inter3: null,
    booleanWait: false,
    booleanMsgValid: false,
    booleanWrite: false,
    intAgentConected : 0,
    SessionID: null,
    token: null,
    intCounter: 0,
    booleanChatEnd: false,
    userData: null,
    stringTypeChat: 'EbChatBaseAutenticacion',
    htmlTimeControl:  `
    <div class='form form-controlhorario' role='form' style='display: block;'>
        <div class="row">
            <div class="col-12 title">
                Recuerda que nuestro horario de atención es:
            </div>
        </div>
        <div class="row">
            <div class="col-12 controlhorario">
                Lunes a Viernes
                <br>7:00 a 12:30
                <br>13:30 a 17:00
            </div>
        </div>
        <div class="row">
            <div class="col-12 subtitle">
                si deseas que te contactemos.
            </div>
        </div>
        <div class="row">
            <div class="col-12 button_datos">
                <button class="cx-btn cx-btn-primary" id="ch_datos">Deja tus datos aquí</button>
            </div>
        </div>
    </div>`
};